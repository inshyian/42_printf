### ft_printf school 42 project

The goal of the project is reimplementation of the default libc function printf.

Conversions supported: diouzxXfF and additional b (print number in base 2).