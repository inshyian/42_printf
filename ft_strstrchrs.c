/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstrchrs.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 18:37:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/02/22 04:20:32 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include "libft.h"

/*
** Function searching chars from *chrs in *s, if found at least one, malloc and
** returns string with all founded chars.
** If found nothing, function returns NULL.
*/

char		*ft_strstrchrs(const char *s, const char *chrs)
{
	char	*ret;
	char	*ret_start;
	char	curr;

	ret_start = NULL;
	while (*chrs)
	{
		curr = *chrs;
		if (ft_strchr(s, curr))
		{
			if (!ret)
			{
				ret = malloc(ft_strlen(chrs));
				ret_start = ret;
			}
			*ret = curr;
			ret++;
		}
		chrs++;
	}
	return (ret_start);
}
